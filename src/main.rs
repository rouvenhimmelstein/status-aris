use std::env;
use std::fs;

use serde::{Deserialize, Serialize};

mod process_handler;
mod updater;
mod downloader;
mod hasher;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Config {
    executable: String,
    files: Vec<RemoteFile>,
}

/// Represents the should state of an file
#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Clone)]
pub struct RemoteFile {
    name: String,
    sha256: String,
    url: String,
}

/// Represents the local variant of an remote file
#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Clone)]
pub struct CachedRemoteFile {
    path: String,
    sha256: String,
}

/// Represents the current local state of an file
#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Clone)]
pub struct LocalFile {
    path: String,
    sha256: String,
}

/// Represents the files to updates
#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Clone)]
pub struct UpdateReport {
    to_update: Vec<CachedRemoteFile>,
    to_add: Vec<CachedRemoteFile>,
    to_remove: Vec<String>,
    needs_update: bool,
}

fn main() {
    if env::args().len() < 1 {
        panic!("Need one parameter which folder to update");
    }

    let config = load_config();

    let cached_remote_files = updater::preload_files(&config);

    let update_report = updater::build_update_report(&cached_remote_files);

    if update_report.needs_update {
        process_handler::end(&config.executable);

        updater::synchronize_files(&config);

        process_handler::start(&config.executable);
    }

    updater::cleanup(&config);
}

fn load_config() -> Config {
    let file_content = fs::read_to_string("status-aris.example.yml").unwrap();
    return serde_yaml::from_str(&file_content).unwrap();
}
