use std::{env, fs};
use std::fs::File;
use std::io::Write;
use std::thread::Builder;

use sha2::{Digest, Sha256, Sha512};

use crate::{CachedRemoteFile, hasher, RemoteFile};

pub fn from_url_to_temp(remote_file: &RemoteFile) -> CachedRemoteFile {
    // create local temp file based on the remote file name
    let mut dir = env::temp_dir();
    dir.push(&remote_file.name);
    let mut temp_file = File::create(&dir).unwrap();

    // download to memory
    let bytes = reqwest::blocking::get(&remote_file.url).unwrap()
        .bytes().unwrap()
        .to_vec();

    // hash temp file data
    let temp_file_sha256 = hasher::sha256(&bytes);

    //build local file name
    let local_file_path = fs::canonicalize(&dir.as_path()).unwrap()
        .into_os_string().to_str().unwrap()
        .to_string();

    // write bytes to local file
    temp_file.write_all(&bytes).unwrap();

    // build return type
    let local_file = CachedRemoteFile {
        path: local_file_path,
        sha256: temp_file_sha256
    };
    return local_file;
}