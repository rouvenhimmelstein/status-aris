use std::{env, fs};
use std::borrow::Borrow;
use std::collections::HashMap;
use std::ffi::OsString;
use std::ops::Deref;
use std::path::{Path, PathBuf};

use crate::{CachedRemoteFile, Config, downloader, hasher, RemoteFile, UpdateReport};

pub fn synchronize_files(config: &Config) {
    println!("update {}", &config.executable)
}

pub fn preload_files(config: &Config) -> Vec<CachedRemoteFile> {
    return config.files.iter()
        .map(|remote_file| downloader::from_url_to_temp(remote_file))
        .collect();
}

pub fn build_update_report(remote_files: &Vec<CachedRemoteFile>) -> UpdateReport {
    let local_files: Vec<String> = fs::read_dir(get_working_dir()).unwrap()
        .map(|entry| entry.unwrap().file_name().into_string().unwrap())
        .collect();

    let to_update = filter_to_update(remote_files, &local_files);
    let to_add = filter_to_add(remote_files, &local_files);
    let to_remove = filter_to_remove(remote_files, &local_files);

    let needs_update = !to_update.is_empty() || !to_update.is_empty() || !to_update.is_empty();

    return UpdateReport {
        to_update,
        to_add,
        to_remove,
        needs_update,
    };
}

/// returns a copy of the specified working directory
fn get_working_dir() -> String {
    let args: Vec<String> = env::args().collect();
    return args[1].clone();
}

/// returns all remote files that should be overwritten from remote
fn filter_to_update(remote_files: &Vec<CachedRemoteFile>, local_files: &Vec<String>) -> Vec<CachedRemoteFile> {
    let to_update: Vec<CachedRemoteFile> = remote_files.iter()
        // files that locally exists can be updated
        .filter(|entry| local_files.contains(&to_file_name(entry)))
        // find the files where the hash does not match
        .filter(|entry| entry.sha256.eq(&hasher::sha256_path(&get_local_file_path(local_files, entry))))
        .map(|entry| entry.clone())
        .collect();
    return to_update;
}

/// returns all remote files that should be added from remote
fn filter_to_add(remote_files: &Vec<CachedRemoteFile>, local_files: &Vec<String>) -> Vec<CachedRemoteFile> {
    let to_add: Vec<CachedRemoteFile> = remote_files.iter()
        // files that remote exists but locally not
        .filter(|entry| !local_files.contains(&to_file_name(entry)))
        .map(|entry| entry.clone())
        .collect();
    return to_add;
}

/// returns all local files that should be removed by remote
fn filter_to_remove(remote_files: &Vec<CachedRemoteFile>, local_files: &Vec<String>) -> Vec<String> {
    let remote_file_names: Vec<String> = remote_files.iter().map(|remote_file| to_file_name(remote_file)).collect();

    let to_add: Vec<String> = local_files.iter()
        // files that exists locally but not remote
        .filter(|entry| !remote_file_names.contains(entry))
        .map(|entry| entry.clone())
        .collect();
    return to_add;
}

/// Finds the matching local path for na remote file
fn get_local_file_path(local_files: &Vec<String>, entry: &CachedRemoteFile) -> PathBuf {
    let local_file_name = to_file_name(entry);

    // find the first matching
    let first_matching: Vec<String> = local_files.iter()
        .map(|x| x.clone())
        .filter(|x| x.eq(&local_file_name))
        .collect();

    // convert a file name to an absolute path that points to a local file
    let first_local_matching_filename = first_matching.first().unwrap();
    let mut absolute_local_path: PathBuf = [get_working_dir(), first_local_matching_filename.to_string()].iter().collect();
    return fs::canonicalize(absolute_local_path).unwrap();
}

/// removes the parent directory path to just leave the filename /temp/example.txt -> example.txt
fn to_file_name(remote_file: &CachedRemoteFile) -> String {
    let full_entry_path = &remote_file.path;
    let file_name = Path::new(full_entry_path).file_name().map(|x| x.to_str()).unwrap();
    return file_name.unwrap().to_string();
}

pub fn cleanup(config: &Config) {}