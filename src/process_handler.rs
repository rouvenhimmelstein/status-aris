pub fn end(name: &String){
    // windows
    // use: https://docs.rs/winapi/0.3.8/winapi/um/processthreadsapi/fn.TerminateProcess.html

    // linux
    // pidof chwp
    // use: kill -9 <PID>


    println!("end {}", &name);
}

pub(crate) fn start(name: &String) {
    println!("start {}", &name);
}